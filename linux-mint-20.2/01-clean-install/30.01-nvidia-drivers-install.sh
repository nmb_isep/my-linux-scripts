#!/bin/bash
#Before reboot
set -e
sudo bash -c "cat > /etc/grub.d/01_enable_vga.conf" << END
cat << EOF 
setpci -s "00:17.0" 3e.b=8 
setpci -s "02:00.0" 04.b=7 
EOF 
END
sudo chmod 755 /etc/grub.d/01_enable_vga.conf
sudo update-grub
sudo reboot