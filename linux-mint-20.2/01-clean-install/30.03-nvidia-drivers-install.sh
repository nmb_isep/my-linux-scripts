#!/bin/bash
#After reboot, fix brightness keyboard shortcuts
sudo bash -c "cat >> /usr/share/X11/xorg.conf.d/nvidia-drm-outputclass-ubuntu.conf" << END
Section "Device"
    Identifier  "Device0"
    Driver      "nvidia"
    VendorName  "NVIDIA Corporation"
    BoardName   "GeForce 320M"
    Option      "RegistryDwords" "EnableBrightnessControl=1"
EndSection
END
reboot