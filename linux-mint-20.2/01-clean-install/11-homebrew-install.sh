#!/bin/bash
#Homebrew
#Installing casks is supported only on macOS
/bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"

#Run these two commands in your terminal to add Homebrew to your PATH:
echo 'eval "$(/home/linuxbrew/.linuxbrew/bin/brew shellenv)"' >> /home/nuno/.profile
eval "$(/home/linuxbrew/.linuxbrew/bin/brew shellenv)"

#Install Homebrew's dependencies if you have sudo access:
sudo apt-get install build-essential

#We recommend that you install GCC:
brew analytics off
brew install gcc